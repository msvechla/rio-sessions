## Builder Image
FROM golang:1 as builder
RUN apt update && apt install -y --no-install-recommends --upgrade openssl
WORKDIR /app

COPY rio-sessions .
COPY rio-gameserver ../rio-gameserver
RUN ls -lr ../rio-gameserver
RUN CGO_ENABLED=0 GO111MODULE=on GOOS=linux go build -a -installsuffix cgo -o app .

## Application Image
FROM alpine:latest

RUN apk update && apk add ca-certificates && rm -rf /var/cache/apk/*

RUN mkdir -p /home/app && \
    addgroup app && \
    adduser -D -G app app

WORKDIR /home/app
COPY --from=builder /app/app rio-sessions
RUN chown -R app:app /home/app
USER app
ENTRYPOINT ["./rio-sessions"]
