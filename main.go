package main

import (
	"fmt"
	"net/http"
	"os"

	log "github.com/sirupsen/logrus"
	"github.com/urfave/cli/v2"
	"gitlab.com/msvechla/rio-sessions/internal/pkg/instrumentation"
	"gitlab.com/msvechla/rio-sessions/internal/pkg/sessions"
)

var (
	flagAMQPURL           string
	flagAMQPUser          string
	flagAMQPPass          string
	flagRedisURL          string
	flagRedisPass         string
	flagPrometheusURL     string
	flagListenAddr        string
	flagTokenCookieDomain string
	flagTokenCookieSecure bool
	flagAllowedOrigin     string
)

func main() {
	log.SetFormatter(&log.JSONFormatter{})

	app := initCLI()
	err := app.Run(os.Args)
	if err != nil {
		log.Fatal(err)
	}

	hostname, err := os.Hostname()
	if err != nil {
		log.Fatalf("Error determining hostname: %s", err)
	}

	sessionsID := fmt.Sprintf("s-%s", hostname)

	// instrumentation
	mc := instrumentation.NewMetricsCollector(sessionsID)
	log.Info("Serving prometheus metrics...")
	go mc.ServeMetrics()

	s, err := sessions.NewServer(
		sessionsID,
		mc,
		flagAMQPURL,
		flagAMQPUser,
		flagAMQPPass,
		flagRedisURL,
		flagRedisPass,
		flagPrometheusURL,
		flagTokenCookieDomain,
		flagTokenCookieSecure,
		flagAllowedOrigin,
	)
	if err != nil {
		log.Fatal(err)
	}

	http.ListenAndServe(flagListenAddr, s)
}

// initCLI initializes the command line
func initCLI() *cli.App {
	app := &cli.App{
		Name:    "rio-sessions",
		Usage:   "",
		Version: "v2.1.1",
	}

	generalFlags := []cli.Flag{
		&cli.StringFlag{
			Name:        "amqpURL",
			Value:       "localhost:5672/",
			Usage:       "AMQP URL to connect to RabbitMQ.",
			EnvVars:     []string{"AMQP_URL"},
			Destination: &flagAMQPURL,
		},
		&cli.StringFlag{
			Name:        "amqpUser",
			Value:       "user",
			Usage:       "AMQP user to connect to RabbitMQ.",
			EnvVars:     []string{"AMQP_USER"},
			Destination: &flagAMQPUser,
		},
		&cli.StringFlag{
			Name:        "amqpPass",
			Value:       "helloworld",
			Usage:       "AMQP Password to connect to RabbitMQ.",
			EnvVars:     []string{"AMQP_PASS"},
			Destination: &flagAMQPPass,
		},
		&cli.StringFlag{
			Name:        "redisURL",
			Value:       "localhost:6379",
			Usage:       "URL to connect to Redis.",
			EnvVars:     []string{"REDIS_URL"},
			Destination: &flagRedisURL,
		},
		&cli.StringFlag{
			Name:        "redisPass",
			Value:       "helloworld",
			Usage:       "Password to authenticate to Redis.",
			EnvVars:     []string{"REDIS_PASS"},
			Destination: &flagRedisPass,
		},
		&cli.StringFlag{
			Name:        "prometheusURL",
			Value:       "http://localhost:9090",
			Usage:       "URL to connect to Prometheus.",
			EnvVars:     []string{"PROMETHEUS_URL"},
			Destination: &flagPrometheusURL,
		},
		&cli.StringFlag{
			Name:        "listenAddr",
			Value:       "0.0.0.0:11111",
			Usage:       "Address the http server will listen on.",
			EnvVars:     []string{"LISTEN_ADDR"},
			Destination: &flagListenAddr,
		},
		&cli.StringFlag{
			Name:        "tokenCookieDomain",
			Value:       "localhost",
			Usage:       "Domain to set on the token cookie.",
			EnvVars:     []string{"TOKEN_COOKIE_DOMAIN"},
			Destination: &flagTokenCookieDomain,
		},
		&cli.BoolFlag{
			Name:        "tokenCookieSecure",
			Value:       false,
			Usage:       "Whether the token cookie should be secure / tls only",
			EnvVars:     []string{"TOKEN_COOKIE_SECURE"},
			Destination: &flagTokenCookieSecure,
		},
		&cli.StringFlag{
			Name:        "allowedOrigin",
			Value:       "http://localhost:8080",
			Usage:       "CORS allowed origin.",
			EnvVars:     []string{"ALLOWED_ORIGIN"},
			Destination: &flagAllowedOrigin,
		},
	}

	app.Flags = append(app.Flags, generalFlags...)

	return app
}
