module gitlab.com/msvechla/rio-sessions

go 1.13

require (
	github.com/ReneKroon/ttlcache v1.7.0
	github.com/ReneKroon/ttlcache/v2 v2.1.0 // indirect
	github.com/go-redis/redis v6.15.9+incompatible // indirect
	github.com/google/uuid v1.3.0
	github.com/gorilla/mux v1.8.0
	github.com/pkg/errors v0.9.1
	github.com/prometheus/client_golang v1.13.0
	github.com/prometheus/common v0.37.0
	github.com/sirupsen/logrus v1.9.0
	github.com/streadway/amqp v1.0.0
	github.com/urfave/cli/v2 v2.11.2
	gitlab.com/msvechla/mux-prometheus v0.0.2
	gitlab.com/msvechla/rio-gameserver v0.0.0-00010101000000-000000000000
	golang.org/x/sys v0.0.0-20220825204002-c680a09ffe64 // indirect
	google.golang.org/protobuf v1.28.1
)

replace gitlab.com/msvechla/rio-gameserver => ../rio-gameserver
