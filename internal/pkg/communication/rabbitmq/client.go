package rabbitmq

import (
	"fmt"
	"net/url"

	"github.com/pkg/errors"
	log "github.com/sirupsen/logrus"
	"github.com/streadway/amqp"
	"gitlab.com/msvechla/rio-gameserver/pkg/communication/messaging"
	"google.golang.org/protobuf/proto"
)

const (
	// ExchangeOpenGames is the exchange where open games are distributed by dispatcherID
	ExchangeOpenGames = "ExchangeOpenGames"
	// QueueOpenGamesRandom is the workqueu where new games are published and picked up at random by any gameserver
	QueueOpenGamesRandom = "OpenGamesRandom"
)

// Client used to communicate with rabbitmq
type Client struct {
	ID       *string
	AMQPUrl  string
	Conn     *amqp.Connection
	Outgoing *amqp.Channel
}

// NewClient returns the amqp client
func NewClient(id *string, amqpURL string, amqpUser string, amqpPass string) (*Client, error) {

	url := fmt.Sprintf("amqp://%s:%s@%s", url.PathEscape(amqpUser), url.PathEscape(amqpPass), amqpURL)

	c := Client{ID: id, AMQPUrl: url}
	return &c, c.Init()
}

// Init initializes the amqp client
func (c *Client) Init() error {
	var err error
	c.Conn, err = amqp.Dial(c.AMQPUrl)

	if err != nil {
		return errors.Wrap(err, "Error establishing AMQP connection")
	}

	// TODO: implememnt correct reconnect logic for rabbitmq
	go func() {
		log.Fatalf("RabbitMQ closed connection, restarting to reconnect: %s", <-c.Conn.NotifyClose(make(chan *amqp.Error)))
	}()

	c.Outgoing, err = c.NewChannel()
	if err != nil {
		return errors.Wrapf(err, "Error establishing AMQP channel Outgoing")
	}

	err = c.Outgoing.ExchangeDeclare(
		ExchangeOpenGames, // name
		"direct",          // type
		true,              // durable
		false,             // auto-deleted
		false,             // internal
		false,             // no-wait
		nil,               // arguments
	)
	if err != nil {
		return errors.Wrapf(err, "Error declaring exchange %s", ExchangeOpenGames)
	}

	err = c.EnsureOpenGamesRandomQueueExists()
	if err != nil {
		return err
	}

	return nil
}

// NewChannel returns a new channel from the amqp connection
func (c *Client) NewChannel() (*amqp.Channel, error) {
	ch, err := c.Conn.Channel()
	if err != nil {
		return nil, errors.Wrap(err, "Error opening AMQP channel")
	}
	return ch, nil
}

// EnsureOpenGamesRandomQueueExists declares the queue for open games waiting to be picked up by a gameserver
func (c *Client) EnsureOpenGamesRandomQueueExists() error {

	q, err := c.Outgoing.QueueDeclare(
		QueueOpenGamesRandom, // name
		true,                 // durable
		false,                // delete when unused
		false,                // exclusive
		false,                // no-wait
		nil,                  // arguments
	)

	if err != nil {
		return errors.Wrapf(err, "Error declaring queue %s", QueueOpenGamesRandom)
	}

	log.Infof("Successfully declared queue %s", q.Name)

	return nil
}

// PublishRandomGameToWorkQueue publishes a new game to the random games work queue
func (c *Client) PublishRandomGameToWorkQueue(message *messaging.Container) error {
	body, err := proto.Marshal(message)
	if err != nil {
		return errors.Wrap(err, "Error marshaling protobuf message")
	}

	c.Outgoing.Publish(
		"",                   // exchange
		QueueOpenGamesRandom, // routing key
		false,                // mandatory
		false,                // immediate
		amqp.Publishing{
			ContentType: "application/protobuf",
			Body:        body,
			AppId:       *c.ID,
		})
	return nil
}

// PublishOpenGameForDispatcher publishes a new game to exchange where it will be picked up by the matching dispatcher
func (c *Client) PublishOpenGameForDispatcher(message *messaging.Container, dispatcherID string) error {
	body, err := proto.Marshal(message)
	if err != nil {
		return errors.Wrap(err, "Error marshaling protobuf message")
	}

	c.Outgoing.Publish(
		ExchangeOpenGames, // exchange
		dispatcherID,      // routing key
		false,             // mandatory
		false,             // immediate
		amqp.Publishing{
			ContentType: "application/json",
			Body:        body,
			AppId:       *c.ID,
		})
	return nil
}
