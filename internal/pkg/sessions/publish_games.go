package sessions

import log "github.com/sirupsen/logrus"

func (s *Server) publishGames() {
	log.Info("Watching new games to publish")
	for {
		select {
		case rg := <-s.randomGames:
			s.amqpClient.PublishRandomGameToWorkQueue(rg.message)
		case sg := <-s.specificGames:
			s.amqpClient.PublishOpenGameForDispatcher(sg.message, sg.dispatcherID)
		}
	}
}
