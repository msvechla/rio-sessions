package sessions

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"
	"time"

	"github.com/ReneKroon/ttlcache"
	"github.com/pkg/errors"
	v1 "github.com/prometheus/client_golang/api/prometheus/v1"
	"github.com/prometheus/common/model"
	log "github.com/sirupsen/logrus"
)

const (
	queryActivePlayers    = "sum(gio_dispatcher_active_players_total)"
	queryActiveSessions   = "sum(gio_dispatcher_active_games_total)"
	cacheTTL              = time.Duration(10 * time.Second)
	cacheKeyStatsResponse = "statsResponse"
)

type statsResponse struct {
	ActivePlayers  int `json:"activePlayers"`
	ActiveSessions int `json:"activeSessions"`
}

// handleStats serves the stats endpoint
func (s *Server) handleStats(w http.ResponseWriter, r *http.Request) {
	s.setupResponse(&w, r)
	if (*r).Method == "OPTIONS" {
		return
	}
	s.enableCors(&w)

	resp, _ := s.statsCache.Get(cacheKeyStatsResponse)
	js, err := json.Marshal(resp)

	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
	}

	w.Write(js)
}

// queryStatsIntValue returns the current value for a query from prometheus
func (s *Server) queryStatsIntValue(query string) (int, error) {
	v1api := v1.NewAPI(*s.prometheusClient)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	result, _, err := v1api.Query(ctx, query, time.Now())
	if err != nil {
		return 0, errors.Wrapf(err, "Querying prometheus with query %s", query)
	}

	switch {
	case result.Type() == model.ValVector:
		vectorVal := result.(model.Vector)

		if len(vectorVal) == 0 {
			return 0, nil
		}
		return strconv.Atoi(vectorVal[0].Value.String())
	default:
		return 0, fmt.Errorf("Error determining active players value for: %s", result)
	}
}

func (s *Server) refreshStatsCache() {

	activePlayers, err := s.queryStatsIntValue(queryActivePlayers)
	if err != nil {
		log.Errorf("Error refreshing stats cache: %s", err)
		return
	}

	activeSessions, err := s.queryStatsIntValue(queryActiveSessions)
	if err != nil {
		log.Errorf("Error refreshing stats cache: %s", err)
		return
	}

	resp := statsResponse{
		ActivePlayers:  activePlayers,
		ActiveSessions: activeSessions,
	}

	log.Info("Successfully refreshed stats cache")

	s.statsCache.Set(cacheKeyStatsResponse, resp)
}

func (s *Server) prepareStatsCache() {
	expirationCallback := func(key string, value interface{}) {
		s.refreshStatsCache()
	}

	s.statsCache = ttlcache.NewCache()
	s.statsCache.SetTTL(cacheTTL)
	s.statsCache.SetExpirationCallback(expirationCallback)
	s.statsCache.SkipTtlExtensionOnHit(true)
	s.statsCache.Set(cacheKeyStatsResponse, statsResponse{})
	s.refreshStatsCache()
}
