package sessions

func (s *Server) routes() {
	s.router.HandleFunc("/health", s.handleHealth)
	s.router.HandleFunc("/sessions/public/newgame", s.handleNewGame).Methods("POST", "OPTIONS")
	s.router.HandleFunc("/sessions/public/token/create/{gameID}", s.handleTokenCreate).Methods("POST", "OPTIONS")
	s.router.HandleFunc("/sessions/public/stats", s.handleStats).Methods("GET", "OPTIONS")
	s.router.HandleFunc("/sessions/internal/token/verify/{gameID}", s.handleTokenVerify).Methods("POST", "OPTIONS")
	s.router.HandleFunc("/sessions/internal/token/delete/{gameID}", s.handleTokenDelete).Methods("POST", "OPTIONS")
	s.router.HandleFunc("/sessions/internal/token/delete-all/{gameID}", s.handleTokenDeleteAll).Methods("POST", "OPTIONS")
}
