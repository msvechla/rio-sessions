package sessions

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"path"
	"regexp"

	"github.com/google/uuid"
	log "github.com/sirupsen/logrus"
	"gitlab.com/msvechla/rio-gameserver/pkg/communication/messaging"
	"google.golang.org/protobuf/proto"
)

// handleTokenCreate handles a request to the player token endpoint
func (s *Server) handleTokenCreate(w http.ResponseWriter, r *http.Request) {
	s.setupResponse(&w, r)
	if (*r).Method == "OPTIONS" {
		return
	}
	s.enableCors(&w)

	gameID := getGameServerIDFromURL(r.URL)

	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		s.writeAndLogError(&w, http.StatusInternalServerError, fmt.Sprintf("Unable to read request body: %s", err))
		return
	}

	var msg messaging.Container
	err = proto.Unmarshal(body, &msg)

	if err != nil {
		s.writeAndLogError(&w, http.StatusBadRequest, fmt.Sprintf("Unable to decode protobuf message: %s", err))
		return
	}

	if valid, err := isValidUsername(msg.GetUserToken().GetUserID()); !valid {
		s.writeAndLogError(&w, http.StatusForbidden, err.Error())
		return
	}

	gameExists, err := s.redisClient.ExistsGame(gameID)
	if err != nil {
		err = s.writeAndLogError(
			&w,
			http.StatusInternalServerError,
			fmt.Sprintf("Error checking games existence: %s", err),
		)
		if err != nil {
			log.Error(err)
		}
		return
	}

	if !gameExists {
		err = s.writeAndLogError(&w, http.StatusForbidden, fmt.Sprintf("GameID %s not found", gameID))
		if err != nil {
			log.Error(err)
		}
		return
	}

	// generate a token and cookie
	cookie, token, isNewToken, err := s.generateTokenForUserInGame(msg.GetUserToken().GetUserID(), gameID)

	if err != nil {
		log.Errorf("Error generating user token: %s", err)
		w.WriteHeader(http.StatusInternalServerError)
	}

	if !isNewToken {
		err = s.writeAndLogError(
			&w,
			http.StatusForbidden,
			fmt.Sprintf("Token for userID %s already exists", msg.GetUserToken().GetUserID()),
		)
		if err != nil {
			log.Error(err)
		}
		return
	}

	resp := messaging.Container{Msg: &messaging.Container_UserToken{UserToken: &messaging.UserToken{Token: token}}}
	pb, _ := proto.Marshal(&resp)

	http.SetCookie(w, cookie)
	w.WriteHeader(http.StatusOK)

	_, err = w.Write(pb)
	if err != nil {
		log.Errorf("Error writing http response: %s", err)
	}
	log.Infof("Issued new token for userID %s", msg.GetUserToken().GetUserID())
}

// generateTokenForUserInGame generates a token for a user in a game, perists it and returns the token, whether a token existed already and the matching cookie
func (s *Server) generateTokenForUserInGame(userID string, gameID string) (*http.Cookie, string, bool, error) {
	// generate a unique token
	token := uuid.New().String()

	// persist the token in redis if it does not exist yet
	isNewToken, err := s.redisClient.SetNXGameTokenForUserInGameID(userID, gameID, token)
	if err != nil {
		return nil, token, isNewToken, err
	}

	// create the cookie
	cookie := http.Cookie{
		Name:     authCookieName(gameID, userID),
		Value:    token,
		Domain:   s.tokenCookieDomain,
		HttpOnly: true,
		Secure:   s.tokenCookieSecure,
		Path:     "/",
		SameSite: http.SameSiteStrictMode,
	}

	return &cookie, token, isNewToken, nil
}

// handleTokenVerify verifies a token for a specific game
func (s *Server) handleTokenVerify(w http.ResponseWriter, r *http.Request) {
	s.setupResponse(&w, r)
	if (*r).Method == "OPTIONS" {
		return
	}
	s.enableCors(&w)

	gameID := getGameServerIDFromURL(r.URL)

	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		s.writeAndLogError(&w, http.StatusInternalServerError, fmt.Sprintf("Unable to read request body: %s", err))
		return
	}

	var msg messaging.Container
	err = proto.Unmarshal(body, &msg)

	if err != nil {
		s.writeAndLogError(&w, http.StatusBadRequest, fmt.Sprintf("Unable to decode protobuf message: %s", err))
		return
	}

	token, err := s.redisClient.GetGameTokenForUserInGameID(msg.GetUserToken().GetUserID(), gameID)
	if err != nil {
		log.Errorf(
			"Error retrieving token for user %s with gameID %s : %s",
			msg.GetUserToken().GetUserID(),
			gameID,
			err,
		)
		w.WriteHeader(http.StatusForbidden)
		return
	}

	if token != msg.GetUserToken().GetToken() {
		log.Infof("Token invalid for user %s with gameID %s", msg.GetUserToken().GetUserID(), gameID)
		w.WriteHeader(http.StatusForbidden)
		return
	}

	log.Infof("Token for user %s with gameID %s verified successfully", msg.GetUserToken().GetUserID(), gameID)
	w.WriteHeader(http.StatusOK)
}

// handleTokenDelete deletes the token for a specific game and player
func (s *Server) handleTokenDelete(w http.ResponseWriter, r *http.Request) {
	s.setupResponse(&w, r)
	if (*r).Method == "OPTIONS" {
		return
	}
	s.enableCors(&w)

	gameID := getGameServerIDFromURL(r.URL)

	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		s.writeAndLogError(&w, http.StatusInternalServerError, fmt.Sprintf("Unable to read request body: %s", err))
		return
	}

	var msg messaging.Container
	err = proto.Unmarshal(body, &msg)

	if err != nil {
		s.writeAndLogError(&w, http.StatusBadRequest, fmt.Sprintf("Unable to decode protobuf message: %s", err))
		return
	}

	err = s.redisClient.RemoveGameTokenForUserInGameID(msg.GetUserToken().GetUserID(), gameID)
	if err != nil {
		log.Errorf("Error deleting token: %s", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	log.Infof("Token for user %s in gameID %s deleted successfully", msg.GetUserToken().GetUserID(), gameID)
	w.WriteHeader(http.StatusOK)
}

// handleTokenDeleteAll deletes all tokens for a specific game
func (s *Server) handleTokenDeleteAll(w http.ResponseWriter, r *http.Request) {
	s.setupResponse(&w, r)
	if (*r).Method == "OPTIONS" {
		return
	}
	s.enableCors(&w)

	gameID := getGameServerIDFromURL(r.URL)

	err := s.redisClient.RemoveAllGameTokensForGameID(gameID)
	if err != nil {
		log.Errorf("Error deleting all tokens for gameID %s: %s", gameID, err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	log.Infof("All tokens for gameID %s deleted successfully", gameID)
	w.WriteHeader(http.StatusOK)
}

// getGameServerIDFromURL parses the gameserver id from the connection url
func getGameServerIDFromURL(requestURL *url.URL) string {
	return path.Base(requestURL.Path)
}

// validateUsername validates the given username
func isValidUsername(username string) (bool, error) {
	re := regexp.MustCompile(`^[A-Za-z0-9]{1,25}$`)

	if !re.MatchString(username) {
		return false, fmt.Errorf(
			"Username should contain only alphanumerical characters, digits, and at most 25 characters",
		)
	}
	return true, nil
}

func authCookieName(gameServerID string, userID string) string {
	return fmt.Sprintf("refinable.io-%s-%s", gameServerID, userID)
}
