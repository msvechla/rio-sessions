package sessions

import (
	"fmt"
	"io/ioutil"
	"net/http"

	"github.com/google/uuid"
	log "github.com/sirupsen/logrus"
	"gitlab.com/msvechla/rio-gameserver/pkg/communication/messaging"
	"gitlab.com/msvechla/rio-gameserver/pkg/communication/redis"
	"google.golang.org/protobuf/proto"
)

func (s *Server) handleNewGame(w http.ResponseWriter, r *http.Request) {
	s.setupResponse(&w, r)
	if (*r).Method == "OPTIONS" {
		return
	}
	s.enableCors(&w)

	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		s.writeAndLogError(&w, http.StatusInternalServerError, fmt.Sprintf("Unable to read request body: %s", err))
		return
	}

	var msg messaging.Container
	err = proto.Unmarshal(body, &msg)

	if err != nil {
		s.writeAndLogError(&w, http.StatusBadRequest, fmt.Sprintf("Unable to decode protobuf message: %s, got: %s, %v", err, body, body))
		return
	}

	if msg.GetOpenGame().GetGameTitle() == "" {
		s.writeAndLogError(&w, http.StatusBadRequest, "Received request without GameTitle")
		return
	}

	if valid, err := isValidGameTitle(msg.GetOpenGame().GetGameTitle()); !valid {
		s.writeAndLogError(&w, http.StatusBadRequest, err.Error())
		return
	}

	// generate a unique game id
	gameID := fmt.Sprintf("sid-%s", uuid.New().String())

	// get the dispatcher with the least running games
	dispatcherID, err := s.redisClient.GetGameServerWithLeastActiveGames()

	// craft the new open game message
	openGameMessage := messaging.Container{
		GameID: gameID,
		Msg: &messaging.Container_OpenGame{
			OpenGame: &messaging.OpenGame{
				GameTitle: msg.GetOpenGame().GetGameTitle(),
				CardDeck:  msg.GetOpenGame().GetCardDeck(),
			},
		},
	}
	openGame := openGame{dispatcherID: dispatcherID, message: &openGameMessage}

	if err != nil {
		switch err.(type) {
		default:
			log.WithField("gameID", gameID).Errorf("Error determining gameserver with least active games: %s, publishing to random games work queue as fallback", err)
			s.randomGames <- &openGame
		case *redis.ErrNoActiveGameServer:
			log.WithField("gameID", gameID).Info("No active game server found, will publish to random games work queue")
			s.randomGames <- &openGame
		}
	} else {
		s.specificGames <- &openGame
		log.WithField("gameID", gameID).WithField("title", openGameMessage.GetOpenGame().GetGameTitle()).WithField("dispatcherID", dispatcherID).Info("Publishing new open game")
	}

	// generate the token and set the cookie accordingly
	cookie, token, _, err := s.generateTokenForUserInGame(messaging.SpecialUserID_HOST.String(), gameID)

	if err != nil {
		log.Errorf("Error generating cookie for host of new game %s: %s", gameID, err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	log.Infof("Issued new token for userID %s", messaging.SpecialUserID_HOST.String())

	http.SetCookie(w, cookie)

	userTokenResponse := messaging.Container{
		GameID: gameID,
		Msg: &messaging.Container_UserToken{
			UserToken: &messaging.UserToken{
				Token: token,
			},
		},
	}

	b, _ := proto.Marshal(&userTokenResponse)

	w.WriteHeader(http.StatusOK)
	_, _ = w.Write(b)
}

// isValidGameTitle validates the given game title
func isValidGameTitle(title string) (bool, error) {
	if len(title) > 60 {
		return false, fmt.Errorf("Game Title should contain at most 60 characters")
	}
	return true, nil
}
