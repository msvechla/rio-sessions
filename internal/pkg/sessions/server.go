package sessions

import (
	"encoding/json"
	"net/http"

	"github.com/ReneKroon/ttlcache"
	"github.com/gorilla/mux"
	"github.com/pkg/errors"
	"github.com/prometheus/client_golang/api"
	promapi "github.com/prometheus/client_golang/api"
	log "github.com/sirupsen/logrus"
	"gitlab.com/msvechla/rio-gameserver/pkg/communication/messaging"
	"gitlab.com/msvechla/rio-gameserver/pkg/communication/redis"
	"gitlab.com/msvechla/rio-sessions/internal/pkg/communication/rabbitmq"
	"gitlab.com/msvechla/rio-sessions/internal/pkg/instrumentation"
)

// Server is the main session server
type Server struct {
	ID                string
	router            *mux.Router
	metricsCollector  *instrumentation.MetricsCollector
	amqpClient        *rabbitmq.Client
	redisClient       *redis.Client
	prometheusClient  *promapi.Client
	statsCache        *ttlcache.Cache
	randomGames       chan *openGame
	specificGames     chan *openGame
	tokenCookieDomain string
	tokenCookieSecure bool
	allowedOrigin     string
}

// openGame represents an open game ready to be published
type openGame struct {
	dispatcherID string
	message      *messaging.Container
}

// NewServer creates a new session server
func NewServer(id string, metricsCollector *instrumentation.MetricsCollector, amqpURL string, amqpUser string, amqpPass string, redisURL string, redisPass string, prometheusURL string, tokenCookieDomain string, tokenCookieSecure bool, allowedOrigin string) (*Server, error) {
	amqpClient, err := rabbitmq.NewClient(&id, amqpURL, amqpUser, amqpPass)
	if err != nil {
		return nil, errors.Wrap(err, "Error creating AMQP client")
	}
	log.Info("Successfully established AMQP connection")

	redisClient, err := redis.NewClient(redisURL, redisPass)
	if err != nil {
		return nil, errors.Wrap(err, "Error creating redis client")
	}
	log.Info("Successfully established redis connection")

	prometheusClient, err := api.NewClient(api.Config{
		Address: prometheusURL,
	})
	if err != nil {
		return nil, errors.Wrap(err, "Error creating prometheus client")
	}

	s := Server{
		router:            mux.NewRouter(),
		metricsCollector:  metricsCollector,
		amqpClient:        amqpClient,
		redisClient:       redisClient,
		prometheusClient:  &prometheusClient,
		randomGames:       make(chan *openGame, 10),
		specificGames:     make(chan *openGame, 10),
		tokenCookieDomain: tokenCookieDomain,
		tokenCookieSecure: tokenCookieSecure,
		allowedOrigin:     allowedOrigin,
	}

	s.prepareStatsCache()

	s.router.Use(s.metricsCollector.MuxProm.Middleware)
	s.routes()

	// watch for new games to publish
	go s.publishGames()

	return &s, nil
}

// ServeHTTP satisfies the http handler interface
func (s *Server) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	s.router.ServeHTTP(w, r)
}

// handleHealth handles requests to the health endpoint
func (s *Server) handleHealth(w http.ResponseWriter, r *http.Request) {
	// TODO: add proper healthcheck
	w.WriteHeader(http.StatusOK)
}

func (s *Server) enableCors(w *http.ResponseWriter) {
	(*w).Header().Set("Access-Control-Allow-Origin", s.allowedOrigin)
}

func (s *Server) setupResponse(w *http.ResponseWriter, req *http.Request) {
	(*w).Header().Set("Access-Control-Allow-Origin", s.allowedOrigin)
	(*w).Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
	(*w).Header().Set("Access-Control-Allow-Headers", "Set-Cookie, Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization")
	(*w).Header().Set("Access-Control-Allow-Credentials", "true")
}

func (s *Server) writeAndLogError(w *http.ResponseWriter, statusCode int, message string) error {
	log.Error(message)

	resp := map[string]string{"error": message}
	js, _ := json.Marshal(resp)

	(*w).WriteHeader(statusCode)

	_, err := (*w).Write(js)
	return errors.Wrap(err, "Error writing http response")
}
