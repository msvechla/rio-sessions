package instrumentation

import (
	"log"
	"net/http"

	"github.com/gorilla/mux"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	muxprom "gitlab.com/msvechla/mux-prometheus/pkg/middleware"
)

const (
	namespace           = "gio"
	gameserverSubsystem = "sessions"
	dispatcherSubsystem = "server"

	// LabelDispatcherID is the label for the dispatcher id
	LabelDispatcherID = "sessions_id"
)

// MetricsCollector contains all data that will be collected
type MetricsCollector struct {
	registerer prometheus.Registerer
	MuxProm    *muxprom.Instrumentation
}

// NewMetricsCollector creates a new metrics collector and registers all metrics
func NewMetricsCollector(dispatcherID string) *MetricsCollector {
	mc := MetricsCollector{
		registerer: prometheus.WrapRegistererWith(prometheus.Labels{LabelDispatcherID: dispatcherID}, prometheus.DefaultRegisterer),
	}

	return &mc
}

// ServeMetrics serves the prometheus metrics so they can get scraped
func (m *MetricsCollector) ServeMetrics() {
	r := mux.NewRouter()

	m.MuxProm = muxprom.NewCustomInstrumentation(true, "mux", "router", prometheus.DefBuckets, map[string]string{}, m.registerer)
	r.Use(m.MuxProm.Middleware)

	r.Handle("/metrics", promhttp.Handler())
	log.Fatal(http.ListenAndServe(":9103", r))
}
